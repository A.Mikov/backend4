<?php
header('Content-Type: text/html; charset=UTF-8');
  
$messages = [];
$errors = [];
$trimed=[];

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

if (!empty($_COOKIE['save'])) {
  setcookie('save', '', 100000);
  $messages[] = 'Спасибо, результаты сохранены.';
}

  $errors['name'] = !empty($_COOKIE['error_name']);
  $errors['email'] = !empty($_COOKIE['error_email']);
  $errors['bd'] = !empty($_COOKIE['error_bd']);
  $errors['pol'] = !empty($_COOKIE['error_pol']);
  $errors['limbs'] = !empty($_COOKIE['error_limbs']);
  $errors['contract'] = !empty($_COOKIE['error_contract']);

  
//Удаление cookies(через установление даты устаревания в прошедшем времени) и вывод сообщений об ошибках заполнения полей
if ($errors['name']) {
  setcookie('error_name', '', 100000);
  $messages[] = '<div ">Заполните имя.</div>';
}
if ($errors['email']) {
  setcookie('error_email', '', 100000);
  $messages[] = '<div ">Заполните почту.</div>';
}
if ($errors['bd']) {
  setcookie('error_bd', '', 100000);
  $messages[] = '<div ">Заполните др.</div>';
}
if ($errors['pol']) {
  setcookie('error_pol', '', 100000);
  $messages[] = '<div ">Заполните пол.</div>';
}
if ($errors['limbs']) {
  setcookie('error_limbs', '', 100000);
  $messages[] = '<div ">Заполните конечности.</div>';
}
if ($errors['contract']) {
  setcookie('error_contract', '', 100000);
  $messages[] = '<div ">Заполните контракт.</div>';
}


$values = array();
$values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
$values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
$values['bd'] = empty($_COOKIE['bd_value']) ? '' : $_COOKIE['bd_value'];
$values['pol'] = empty($_COOKIE['pol_value']) ? '' : $_COOKIE['pol_value'];
$values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
$values['contract'] = empty($_COOKIE['contract_value']) ? '' : $_COOKIE['contract_value'];
$values['superpowers'] = array();
$values['superpowers'][0] = empty($_COOKIE['superpowers_value_0']) ? '' : $_COOKIE['superpowers_value_0'];
$values['superpowers'][1] = empty($_COOKIE['superpowers_value_1']) ? '' : $_COOKIE['superpowers_value_1'];
$values['superpowers'][2] = empty($_COOKIE['superpowers_value_2']) ? '' : $_COOKIE['superpowers_value_2'];
$values['superpowers'][3] = empty($_COOKIE['superpowers_value_3']) ? '' : $_COOKIE['superpowers_value_3'];
include('form.php');
}

else {
  $errors = FALSE;

  //проверка корректности заполненных полей
  if ((empty($_POST['name']))) {
    setcookie('error_name', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60 * 12);
  }


  if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $_POST['email'])) {
    setcookie('error_email', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60 * 12);
  }
  
  
  if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['bd'])) {
    setcookie('error_bd', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('bd_value', $_POST['bd'], time() + 30 * 24 * 60 * 60 * 12);
  }
  

  if (!preg_match('/^[MFO]$/', $_POST['pol'])) {
    setcookie('error_pol', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('pol_value', $_POST['pol'], time() + 30 * 24 * 60 * 60 * 12);
  }


  if (!preg_match('/^[2-6]$/', $_POST['limbs'])) {
    setcookie('error_limbs', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60 * 12);
  }


  if (!isset($_POST['contract'])) {
    setcookie('error_contract', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('contract_value', $_POST['contract'], time() + 30 * 24 * 60 * 60 * 12);
  }

  setcookie('superpowers_value_0', '', 100000);
  setcookie('superpowers_value_1', '', 100000);
  setcookie('superpowers_value_2', '', 100000);
  setcookie('superpowers_value_3', '', 100000);

  foreach($_POST['superpowers'] as $super) {
    setcookie('superpowers_value_' . ($super - 1), 'true', time() + 30 * 24 * 60 * 60 * 12);
    }
 
  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('error_name', '', 100000);
    setcookie('error_email', '', 100000);
    setcookie('error_pol', '', 100000);
    setcookie('error_limbs', '', 100000);
    setcookie('error_bd', '', 100000);
    setcookie('error_contract', '', 100000);
  }

  setcookie('save', '1');

  header('Location: index.php');

  $user = 'u36993';
 $pass = '3932198';
 $db = new PDO('mysql:host=localhost;dbname=u36993', $user, $pass, array(PDO::ATTR_PERSISTENT => true));


  try {
    $stmt1 = $db->prepare("INSERT INTO FORM SET name = ?, email = ?, bd = ?, pol= ? , kon = ?, bio = ?");
    $stmt1 -> execute([$_POST['name'], $_POST['email'], $_POST['bd'], $_POST['pol'], $_POST['limbs'], $_POST['bio']]);
    $stmt2 = $db->prepare("INSERT INTO user_sups SET user_id = ?, id_sup = ?");
    $lastId = $db->lastInsertId();
    foreach ($_POST['superpowers'] as $super)
      $stmt2 -> execute([$lastId, $super]);
  }
  catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
  }

  header('Location: ?save=1');
}

 
